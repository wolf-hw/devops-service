package io.choerodon.devops.app.service;

import io.choerodon.devops.infra.dto.ApplicationVersionReadmeDTO;

/**
 * Created by Sheep on 2019/7/12.
 */
public interface ApplicationVersionReadmeService {

    ApplicationVersionReadmeDTO baseCreate(ApplicationVersionReadmeDTO applicationVersionReadmeDTO);

}
