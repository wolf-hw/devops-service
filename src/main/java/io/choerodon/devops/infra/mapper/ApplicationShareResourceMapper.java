package io.choerodon.devops.infra.mapper;

import io.choerodon.devops.infra.dto.ApplicationShareResourceDTO;
import io.choerodon.mybatis.common.Mapper;

/**
 * Creator: ChangpingShi0213@gmail.com
 * Date:  15:13 2019/6/28
 * Description:
 */
public interface ApplicationShareResourceMapper extends Mapper<ApplicationShareResourceDTO> {

}
