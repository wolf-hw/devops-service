package io.choerodon.devops.infra.mapper;

import io.choerodon.devops.infra.dto.ApplicationVersionReadmeDTO;
import io.choerodon.mybatis.common.Mapper;

/**
 * Creator: Runge
 * Date: 2018/6/19
 * Time: 11:17
 * Description:
 */
public interface ApplicationVersionReadmeMapper extends Mapper<ApplicationVersionReadmeDTO> {
}
