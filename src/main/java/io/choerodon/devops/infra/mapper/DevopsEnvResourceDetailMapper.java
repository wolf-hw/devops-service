package io.choerodon.devops.infra.mapper;

import io.choerodon.devops.infra.dto.DevopsEnvResourceDetailDTO;
import io.choerodon.mybatis.common.Mapper;

/**
 * Created by younger on 2018/4/24.
 */
public interface DevopsEnvResourceDetailMapper extends Mapper<DevopsEnvResourceDetailDTO> {

}
