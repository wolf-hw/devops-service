/**
 * @author ale0720@163.com
 * @date 2019-05-28 13:47
 */

export const STEP_FLAG = {
  IMPORT_ORIGIN: 'step0',
  LANGUAGE_SELECT: 'step1',
  PERMISSION_RULE: 'step2',
  CONFORM_INFO: 'step3',
};

export const REPO_TYPE = {
  REPO_GITHUB: 'github',
  REPO_GITLAB: 'gitlab',
};
