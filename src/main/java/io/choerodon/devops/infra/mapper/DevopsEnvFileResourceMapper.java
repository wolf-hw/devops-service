package io.choerodon.devops.infra.mapper;

import io.choerodon.devops.infra.dto.DevopsEnvFileResourceDTO;
import io.choerodon.mybatis.common.Mapper;

/**
 * Creator: Runge
 * Date: 2018/7/25
 * Time: 15:42
 * Description:
 */
public interface DevopsEnvFileResourceMapper extends Mapper<DevopsEnvFileResourceDTO> {
}
