package io.choerodon.devops.infra.mapper;

import io.choerodon.devops.infra.dto.ApplicationVersionValueDTO;
import io.choerodon.mybatis.common.Mapper;

public interface ApplicationVersionValueMapper extends Mapper<ApplicationVersionValueDTO> {
}
